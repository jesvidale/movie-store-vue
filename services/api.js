const endPoint = 'https://api.themoviedb.org/3'

export const api = {
  requestCategories: async () => {
    const resp = await fetch(`${endPoint}/genre/movie/list?api_key=${process.env.apiKey}&language=es-ES`, {
      method: 'GET',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    }).catch((error) => {
      console.error('An error occurred while requestCategories request:' + error.message);
    });
    return resp.json()
  },
  
  requestMostPopular: async () => {
    const resp = await fetch(`${endPoint}/movie/popular?api_key=${process.env.apiKey}&language=es-ES`, {
      method: 'GET',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    }).catch((error) => {
      console.error('An error occurred while requestMostPopular request:' + error.message);
    });
    return resp.json()
  },
  
  requestByGenre: async(genreID) => {
    const resp = await fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${process.env.apiKey}&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${genreID}&with_watch_monetization_types=flatrate`, {
      method: 'GET',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    }).catch((error) => {
      console.error('An error occurred while requestByGenre request:' + error.message);
    });
    return resp.json()
  },
  
  requestMovieDetail: async (movieID) => {
    const resp = await fetch(`${endPoint}/movie/${movieID}?api_key=${process.env.apiKey}&language=es-ES`, {
      method: 'GET',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    }).catch((error) => {
      console.error('An error occurred while requestMovieDetail request:' + error.message);
    });
    return resp.json()
  },

  requestMovieByQuery: async (query) => {
    const resp = await fetch(`${endPoint}/search/multi?api_key=${process.env.apiKey}&language=es-ES&query=${query}&page=1&include_adult=false`, {
      method: 'GET',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
    }).catch((error) => {
      console.error('An error occurred while requestMovieByQuery request:' + error.message);
    });
    return resp.json()
  }
}
