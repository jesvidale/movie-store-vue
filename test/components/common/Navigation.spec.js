import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Navigation from '@/components/common/Navigation'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Navigation.vue', () => {
  let factory
  let store

  beforeEach(() => {

    factory = config => {
      return mount(Navigation, {
        store,
        localVue,
        stubs: ['ion-icon', 'nuxt-link'],
        ...config
      })
    }

    jest.resetModules()
    jest.clearAllMocks()
  })

  it('is a Vue instance', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should open mobile nav when click on NavButton', () => {
    const wrapper = factory()
    wrapper.find('.menu').trigger('click')
    expect(wrapper.find('.navigation-links-wrapper').isVisible()).toBeTruthy()
  })
})
