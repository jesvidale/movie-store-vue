export class Categories {
  constructor(categories) {
    this.categories = categories
  }

  get orderByName() {
    return this.orderCategories()
  }

  orderCategories() {
    return this.categories.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }
}

export class MovieResume {
  constructor(movie) {
    this.id = movie.id
    this.title = movie.title ? movie.title : ''
    this.vote = movie.vote_average
    this.released_on = movie.release_date
    this.overview = movie.overview
    this.category_ids = movie.genre_ids
  }
}

export class MovieDetail {
  constructor(movie) {
    this.title = movie.title
    this.adult = movie.adult
    this.id = movie.id
    this.overview = movie.overview
    this.production_countries = movie.production_countries
    this.released_on = movie.release_date
    this.revenue = movie.revenue
    this.status = movie.status
    this.vote = movie.vote_average
    this.categories = movie.genres
  }

  get getCategoriesByName() {
    return this.filterByName('categories')
  }

  get getCountriesByName() {
    return this.filterByName('production_countries')
  }

  filterByName(prop) {
    return this[prop].map(el => el.name)
  }
}
