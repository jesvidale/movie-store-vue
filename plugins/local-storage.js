import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'capitole',
    paths: ['categories', 'order']
  })(store)
}
