export const state = () => ({
  list: [],
  size: 0
})

export const getters = {
  getOrder: state => state.list,
  getOrderSize: state => state.size
}

export const actions = {
  // item = { title, id }
  addToOrder({ state, commit }, item) {
    if(Object.prototype.hasOwnProperty.call(item, 'id')) {
      if (state.list.find(el => el.id === item.id)) {
        commit('INCREASE_EXISTING_ORDER', item.id)
      } else {
        commit('ADD_TO_ORDER', { id: item.id, title: item.title, amount: 1 })
      }
    }
  },
  decreaseFromOrder({ state, commit }, item) {
    if(Object.prototype.hasOwnProperty.call(item, 'id')) {
      const element = state.list.find(el => el.id === item.id)
      if (element.amount > 1) {
        commit('DECREASE_EXISTING_ORDER', item.id)
      }
    }
  },
  removeFromOrder({ state, commit }, item) {
    if(Object.prototype.hasOwnProperty.call(item, 'id')) {
      if (state.list.find(el => el.id === item.id)) {
        commit('REMOVE_FROM_ORDER', item.id)
      }
    }
  }
}

export const mutations = {
  INCREASE_EXISTING_ORDER(state, itemID) {
    const res = state.list.find(el => el.id === itemID)
    res.amount += 1
    state.size += 1
  },
  ADD_TO_ORDER(state, item) {
    state.list.push(item)
    state.size += 1
  },
  DECREASE_EXISTING_ORDER(state, itemID) {
    const res = state.list.find(el => el.id === itemID)
    res.amount -= 1
    state.size -= 1
  },
  REMOVE_FROM_ORDER(state, itemID) {
    const res = state.list.find(el => el.id === itemID)
    state.size -= res.amount
    state.list = state.list.filter(el => el.id !== itemID)
  }
}
